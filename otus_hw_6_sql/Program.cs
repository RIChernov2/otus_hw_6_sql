﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace otus_hw_6_sql
{
    class Program
    {
        static readonly string s_defaultString = "Server=localhost;Port=5432;User Id=otusUser;Password=otus12345;Database=otusHW7;";
        static void Main(string[] args)
        {
            string connectionString = "";

            bool correctConnection;
            do
            {
                Console.WriteLine($"Введите строку подключения, например:\n{s_defaultString}\n");
                connectionString = Console.ReadLine();
                if ( connectionString == "" ) connectionString = s_defaultString;
                correctConnection = Creator.ChechConnection(connectionString);
                if ( !correctConnection ) Console.WriteLine("Не удалось подключиться!\n");
            }
            while ( !correctConnection );

            Creator.CreateTables(connectionString);
            Printer.PrintTablesData(connectionString);
            TableChanger changer = new(connectionString);
            changer.Start();
        }

    }
}
