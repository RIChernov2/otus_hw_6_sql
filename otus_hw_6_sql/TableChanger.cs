﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_hw_6_sql
{
    internal class TableChanger
    {
        private readonly string _connectionString;
        public TableChanger(string connectionString)
        {
            _connectionString = connectionString;
        }
        public void Start()
        {
            string input = "";
            do
            {
                string message =
                    @" 
                    Выберите действие:
                    1 - добавить строку в таблицу users
                    2 - добавить строку в таблицу users_data
                    3 - добавить строку в таблицу users_contacts

                    5 - вывести данные таблиц

                    0 - выход
                ";
                Console.WriteLine(message);
                input = Console.ReadLine();
                TryWorking(input);
                if ( input == "5" ) Printer.PrintTablesData(_connectionString);

            }
            while ( input != "0" );

        }

        private void TryWorking(string input)
        {
            if ( input != "1" && input != "2" && input != "3") return;
            TypeFormatMessage(input);
            string input2 = Console.ReadLine();
            string[] args = input2.Split(" ");
            TryInsert(args, input);
        }
        private void TypeFormatMessage(string input)
        {
            string name = "";
            string message = "";
            if ( input == "1" )
            {
                name = "users";
                message = $"Для добавления строки в таблицу {name}\nвведите через пробел(string login, string password)";
            }
            else if ( input == "2" )
            {
                name = "users_data";
                message = $"Для добавления строки в таблицу {name}\nведите через пробел(string name, string second_name, string showing_nick, int user_id)";
            }
            else if ( input == "3" )
            {
                name = "users_contacts";
                message = $"Для добавления строки в таблицу {name}\nвведите через пробел(string email, string phone, int user_id)";
            }
            else
            {
                throw new Exception("Непредвиденная ошибка ввода");
            }
            Console.WriteLine($"\n{message}\n");
        }

        private void TryInsert(string[] args, string input)
        {
            try
            {
                if ( input == "1" )
                {
                    InsertIntoUsers(args[0], args[1]);
                }
                else if ( input == "2" )
                {
                    InsertIntoUsersData(args[0], args[1], args[2], int.Parse(args[3]));
                }
                else if ( input == "3" )
                {
                    InsertIntoUsersContacts(args[0], args[1], int.Parse(args[2]));
                }
                else
                {
                    Console.WriteLine("Непредвиденная ошибка ввода");
                }
            }
            catch
            {
                Console.WriteLine(">>> Неверный формат данных");
            }

        }

        public void InsertIntoUsers(string login, string password)
        {
            using var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var sql =
                $@"

                    INSERT INTO {Creator.MySch}.users (login, password)
                    VALUES (:login, :password);
                ";

            using var cmd = new NpgsqlCommand(sql, connection);
            var parameters = cmd.Parameters;
            parameters.Add(new NpgsqlParameter("login", $"{login}"));
            parameters.Add(new NpgsqlParameter("password", $"{password}"));
            try
            {
                cmd.ExecuteNonQuery();
                PrintSuccessMessage();
            }
            catch (Exception ex)
            {
                PrintExceptioMessage(ex.Message);
            }
        }

        public void InsertIntoUsersData(string name, string second_name, string showing_nick, int user_id)
        {
            using var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var sql =
                $@"

                    INSERT INTO {Creator.MySch}.users_data (name, second_name, showing_nick, user_id)
                    VALUES (:name, :second_name, :showing_nick, @user_id);
                ";

            using var cmd = new NpgsqlCommand(sql, connection);
            var parameters = cmd.Parameters;
            parameters.Add(new NpgsqlParameter("name", $"{name}"));
            parameters.Add(new NpgsqlParameter("second_name", $"{second_name}"));
            parameters.Add(new NpgsqlParameter("showing_nick", $"{showing_nick}"));
            parameters.Add(new NpgsqlParameter("user_id", user_id));
            try
            {
                cmd.ExecuteNonQuery();
                PrintSuccessMessage();
            }
            catch ( Exception ex )
            {
                PrintExceptioMessage(ex.Message);
            }
        }

        public void InsertIntoUsersContacts(string email, string phone, int user_id)
        {
            using var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var sql =
                $@"

                    INSERT INTO {Creator.MySch}.users_contacts (email, phone, user_id)
                    VALUES (:email, :phone, @user_id);
                ";

            using var cmd = new NpgsqlCommand(sql, connection);
            var parameters = cmd.Parameters;
            parameters.Add(new NpgsqlParameter("email", $"{email}"));
            parameters.Add(new NpgsqlParameter("phone", $"{phone}"));
            parameters.Add(new NpgsqlParameter("user_id", user_id));
            try
            {
                cmd.ExecuteNonQuery();
                PrintSuccessMessage();
            }
            catch ( Exception ex )
            {
                PrintExceptioMessage(ex.Message);
            }
        }

        private void PrintExceptioMessage(string message)
        {
            var s = new string('*', 50);
            Console.WriteLine($"\n{s}ОШИБКА{s}");
            Console.WriteLine(message);
            Console.WriteLine($"{s}******{s}\n");
        }

        private void PrintSuccessMessage() => Console.WriteLine("\n>>> Строка добавлена");
    }
}
