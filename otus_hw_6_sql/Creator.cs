﻿using Npgsql;

namespace otus_hw_6_sql
{
    internal static class Creator
    {
        public static readonly string MySch = "schema_chernov";

        public static bool ChechConnection(string connectionString)
        {
            try
            {
                using var connection = new NpgsqlConnection(connectionString);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static void CreateTables(string connectionString)
        {
            using var connection = new NpgsqlConnection(connectionString);
            connection.Open();
            CreateTables(connection);
            FillTables(connection);

            System.Console.WriteLine("\nТаблицы созданы!\n");
        }

        private static void CreateTables(NpgsqlConnection connection)
        {
            var sqlShema =
                $@"
                    DROP    SCHEMA  IF EXISTS {MySch} CASCADE;
                    CREATE  SCHEMA  {MySch};
                    SET     SCHEMA  '{MySch}';
                ";

            using ( var cmd = new NpgsqlCommand(sqlShema, connection) )
            {
                cmd.ExecuteNonQuery();
            }



            // create users table
            string current = "users";
            string usersTable = current;
            var tableSQL =
                $@"
                    CREATE SEQUENCE {current}_id_seq;

                    CREATE TABLE {current}
                    (
                        id          BIGINT                  NOT NULL    DEFAULT NEXTVAL('{current}_id_seq'),
                        login       CHARACTER VARYING(20)   NOT NULL,
                        password    CHARACTER VARYING(20)   NOT NULL,

                        CONSTRAINT {current}_pkey PRIMARY KEY(id),
                        CONSTRAINT {current}_login_unique UNIQUE(login)

                    );

                    CREATE UNIQUE INDEX {current}_login_idx ON {current}(lower(login));
                ";

            using ( var cmd = new NpgsqlCommand(tableSQL, connection) )
            {
                cmd.ExecuteNonQuery();
            }



            // create users_data table
            current = "users_data";
            tableSQL =
                $@"
                    CREATE SEQUENCE {current}_id_seq;

                    CREATE TABLE {current}
                    (
                        id              BIGINT                  NOT NULL    DEFAULT NEXTVAL('{current}_id_seq'),
                        name            CHARACTER VARYING(20)   NOT NULL,
                        second_name     CHARACTER VARYING(20),
                        showing_nick    CHARACTER VARYING(20)   NOT NULL,
                        user_id         BIGINT                  NOT NULL,


                        CONSTRAINT {current}_pkey PRIMARY KEY(id),
                        CONSTRAINT {current}_fk_{usersTable}_id FOREIGN KEY(user_id) REFERENCES {usersTable}(id) ON DELETE CASCADE,
                        CONSTRAINT {current}_showing_nick_unique UNIQUE(showing_nick),

                        CONSTRAINT {current}_user_id_unique UNIQUE(user_id)

                    );

                    CREATE UNIQUE INDEX {current}_showing_nick_idx ON {current}(lower(showing_nick));
                ";

            //deferrable initially deferred

            using ( var cmd = new NpgsqlCommand(tableSQL, connection) )
            {
                cmd.ExecuteNonQuery();
            }


            // create users_contacts table
            current = "users_contacts";
            tableSQL =
                $@"
                    CREATE SEQUENCE {current}_id_seq;

                    CREATE TABLE {current}
                    (
                        id          BIGINT                  NOT NULL    DEFAULT NEXTVAL('{current}_id_seq'),
                        email       CHARACTER VARYING(50),
                        phone       CHARACTER VARYING(20),
                        user_id     BIGINT                  NOT NULL,



                        CONSTRAINT {current}_pkey PRIMARY KEY(id),
                        CONSTRAINT {current}_fk_{usersTable}_id FOREIGN KEY(user_id) REFERENCES {usersTable}(id) ON DELETE CASCADE,
                        CONSTRAINT {current}_user_id_unique UNIQUE(user_id)

                    );
                ";

            using ( var cmd = new NpgsqlCommand(tableSQL, connection) )
            {
                cmd.ExecuteNonQuery();
            }


        }

        private static void FillTables(NpgsqlConnection connection)
        {

            // set users
            var setSQL =
                @"
                    INSERT INTO users (login, password)
                    VALUES
                    ('RICh', 'qwerty12345'),
                    ('BGood', 'johnnymusician'),
                    ('Mark', 'TwainIsClassic'),
                    ('Bond', 'Fleming007'),
                    ('DarthVader', 'feelthepower');
                ";

            using ( var cmd = new NpgsqlCommand(setSQL, connection) )
            {
                cmd.ExecuteNonQuery();
            }


            // set users_data
            setSQL =
                @"
                    INSERT INTO users_data (name, second_name, showing_nick, user_id)
                    VALUES
                    ('Роман', 'Ч', 'RIChernov', 1),
                    ('Джон', '', 'McFly', 2),
                    ('Гек', 'Любимов', 'Sawyer', 3),
                    ('Шон', 'Простошон', 'Agent007', '4'),
                    ('Anakin', 'Skywalker', 'CuteDude', '5');
                ";

            using ( var cmd = new NpgsqlCommand(setSQL, connection) )
            {
                cmd.ExecuteNonQuery();
            }


            // set users_contacts
            setSQL =
                @"
                    INSERT INTO users_contacts (email, phone,user_id)
                    VALUES
                    ('r.i.chernov@mail.ru', '322-223-322', 1),
                    ('matters@gmail.com', '1955-1985-2015', 2),
                    ('', '8-10-1-1835-1910', 3),
                    ('everygirl@likesme.com', '+7-495-777-77-77', 4),
                    ('Tatooine@gmail.com', '', '5');
                ";

            using ( var cmd = new NpgsqlCommand(setSQL, connection) )
            {
                cmd.ExecuteNonQuery();
            }
        }

    }
}
